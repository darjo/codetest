package com.pierceecom.blog.controller;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Testing Blog controller using json not entities, database is recreated before each method (rollback tests are not available in full web environment)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BlogTestIntegr {

    private static final String POST_1 = "{\"title\":\"First title\",\"content\":\"First content\"}";
    private static final String POST_1_EDITED = "{\"title\":\"First title\",\"content\":\"First content but edited\"}";
    private static final String POST_1_NOT_VALID = "{\"title\":\"First title\"}";
    private static final String POST_1_NOT_VALID_CUSTOM_RULE = "{\"title\":\"first title\",\"content\":\"First content but edited\"}";
    private static final String CUSTOM_RULE_MESSAGE = "{\"errors\":[{\"detail\":\"title should begins with capital letter or digit\",\"source\":{\"pointer\":\"data/attributes/title-field\"}}]}";
    private static final String POST_2 = "{\"title\":\"Second title\",\"content\":\"Second content\"}";
    private static final String POST_2_EDITED = "{\"title\":\"Second title\",\"content\":\"Second content but edited\"}";
    private static final String POSTS_URI = "/posts";
    private static final String SIMPLE_LIST_URI = "/posts/simple";
    private static final String FULL_URI = "http://localhost:%d/blog-web%s";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void blogWithoutPosts() throws JSONException {

        final ResponseEntity<String> entity = restTemplate.getForEntity(POSTS_URI, String.class);

        JSONAssert.assertEquals("{posts: [], meta: {}}", entity.getBody(), false);
    }

    @Test
    public void addPosts() throws JSONException {

        ResponseEntity<String> response = POST(POSTS_URI, POST_1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/1"));
        JSONAssert.assertEquals(POST_1, response.getBody(), false);

        response = POST(POSTS_URI, POST_2);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/2"));
        JSONAssert.assertEquals(POST_2, response.getBody(), false);
    }

    @Test
    public void addInvalidPost() {

        final ResponseEntity<String> response = POST(POSTS_URI, POST_1_NOT_VALID);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void editPost() throws JSONException {

        ResponseEntity<String> response = POST(POSTS_URI, POST_1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/1"));
        JSONAssert.assertEquals(POST_1, response.getBody(), false);

        response = PUT(POSTS_URI + "/1", POST_1_EDITED);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/1"));
        JSONAssert.assertEquals(POST_1_EDITED, response.getBody(), false);

        response = GET(POSTS_URI + "/1");
        JSONAssert.assertEquals(POST_1_EDITED, response.getBody(), false);
    }

    @Test
    public void editNotExistingPost() throws JSONException {

        ResponseEntity<String> response = POST(POSTS_URI, POST_1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/1"));
        JSONAssert.assertEquals(POST_1, response.getBody(), false);

        response = PUT(POSTS_URI + "/2", POST_2_EDITED);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void editNotValidPost() throws JSONException {

        ResponseEntity<String> response = POST(POSTS_URI, POST_1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/1"));
        JSONAssert.assertEquals(POST_1, response.getBody(), false);

        response = PUT(POSTS_URI + "/1", POST_1_NOT_VALID);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void editPostThatBreaksCustomValidationRule() throws JSONException {

        ResponseEntity<String> response = POST(POSTS_URI, POST_1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(getResponseLocation(response)).isEqualTo(String.format(FULL_URI, port, POSTS_URI + "/1"));
        JSONAssert.assertEquals(POST_1, response.getBody(), false);

        response = PUT(POSTS_URI + "/1", POST_1_NOT_VALID_CUSTOM_RULE);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        JSONAssert.assertEquals(CUSTOM_RULE_MESSAGE, response.getBody(), false);
    }

    @Test
    public void getPost() throws JSONException {

        POST(POSTS_URI, POST_1);
        POST(POSTS_URI, POST_2);

        ResponseEntity<String> response = GET(POSTS_URI + "/1");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals(POST_1, response.getBody(), false);

        response = GET(POSTS_URI + "/2");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals(POST_2, response.getBody(), false);
    }

    @Test
    public void getNotExistingPost() {

        POST(POSTS_URI, POST_1);

        final ResponseEntity<String> response = GET(POSTS_URI + "/2");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getAllPosts() throws JSONException {

        POST(POSTS_URI, POST_1);
        POST(POSTS_URI, POST_2);

        final ResponseEntity<String> response = GET(POSTS_URI);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals("{ posts: [" + POST_1 + "," + POST_2 + "], meta: { total_pages: 1 }}", response.getBody(), false);
    }

    @Test
    public void deletePosts() throws JSONException {

        POST(POSTS_URI, POST_1);
        POST(POSTS_URI, POST_2);

        ResponseEntity<String> response = DELETE(POSTS_URI + "/1");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        response = GET(POSTS_URI + "/1");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        response = DELETE(POSTS_URI + "/2");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        response = GET(POSTS_URI + "/2");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        response = GET(SIMPLE_LIST_URI);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals("[]", response.getBody(), false);

    }

    @Test
    public void deleteNotExistingPost() {

        POST(POSTS_URI, POST_1);

        final ResponseEntity<String> response = DELETE(POSTS_URI + "/2");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    private String getResponseLocation(final ResponseEntity<?> response) {
        if (response.getStatusCode() == HttpStatus.CREATED) {
            return response.getHeaders().get(HttpHeaders.LOCATION).get(0);
        } else {
            return null;
        }
    }

    private HttpHeaders defaultHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }


    private ResponseEntity<String> POST(final String uri, final String json) {
        final HttpEntity<String> entity = new HttpEntity<>(json, defaultHeaders());
        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    private ResponseEntity<String> PUT(final String uri, final String json) {
        final HttpEntity<String> entity = new HttpEntity<>(json, defaultHeaders());
        return restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
    }

    private ResponseEntity<String> GET(final String uri) {
        return restTemplate.getForEntity(uri, String.class);
    }

    private ResponseEntity<String> DELETE(final String uri) {
        return restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
    }

}
