import DS from 'ember-data';

export default DS.RESTSerializer.extend({

  normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
    payload = {
          post: payload
    };

    return this._super(store, primaryModelClass, payload, id, requestType);
  },

  serializeIntoHash: function(hash, type, record, options) {
    options = options ? options : {};
    options.includeId = true;
    Ember.assign(hash, this.serialize(record, options));
  }

});
