CREATE TABLE post (
  id BIGSERIAL PRIMARY KEY,
  title varchar(250),
  content varchar(1024)
);