package com.pierceecom.blog.service;

import com.pierceecom.blog.model.Post;
import com.pierceecom.blog.model.Posts;

public interface PostService {

    Posts findPage(Integer page, Integer perPage);

    Posts findAll();

    Iterable<Post> listAll();

    Post insert(Post post);

    Post update(Long postId, Post post);

    Post findById(Long postId);

    void delete(Long postId);

}
