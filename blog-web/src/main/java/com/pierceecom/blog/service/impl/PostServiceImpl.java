package com.pierceecom.blog.service.impl;

import com.pierceecom.blog.exception.NoContentException;
import com.pierceecom.blog.exception.PostNotFoundException;
import com.pierceecom.blog.model.Post;
import com.pierceecom.blog.model.Posts;
import com.pierceecom.blog.repository.PostRepository;
import com.pierceecom.blog.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.stream.IntStream;

@Transactional
@Service
public class PostServiceImpl implements PostService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private Environment environment;

    @PersistenceContext
    private EntityManager em;

    @Override
    public Posts findPage(final Integer page, final Integer perPage) {

        final Posts posts = new Posts();

        final Page<Post> postPage = postRepository.findAllByOrderByIdDesc(PageRequest.of(page - 1, perPage));
        posts.setPosts(postPage.getContent());
        posts.getMeta().setTotalPages(postPage.getTotalPages());

        return posts;
    }

    @Override
    public Posts findAll() {

        final Posts posts = new Posts();
        posts.setPosts(listAll());
        posts.getMeta().setTotalPages(1);
        return posts;
    }

    @Override
    public Iterable<Post> listAll() {
        return postRepository.findAllByOrderByIdDesc();
    }

    @Override
    public Post insert(final Post post) {

        final Post savedPost = postRepository.save(post);
        logger.debug("Post {} added with id {}", post, savedPost.getId());

        return savedPost;
    }

    @Override
    public Post update(final Long postId, final Post post) {

        final Post entity = postRepository.findById(postId).orElseThrow(PostNotFoundException::new);
        logger.trace("Post with id {} found: {}", postId, post);

        entity.setTitle(post.getTitle());
        entity.setContent(post.getContent());

        return entity;
    }

    @Override
    public Post findById(final Long postId) {

        return postRepository.findById(postId).orElseThrow(NoContentException::new);
    }

    @Override
    public void delete(final Long postId) {

        final Post post = postRepository.findById(postId).orElseThrow(PostNotFoundException::new);
        postRepository.delete(post);
    }

    @Override
    public void afterPropertiesSet() {

        if (environment.containsProperty("populate-db")) {

            logger.debug("Populating database");
            IntStream.range(0, 100).forEach(i -> {
                postRepository.save(new Post(null, Integer.toString(10 * i + 1) + ". " + "TEMPERATURY BLISKIE REKORDOWYM?", "Na granicy skillu modelu ECMWF pojawiły się wartości temperatur bliskie rekordom absolutnym sierpnia. Dla Ziemi Lubuskiej model przepowiada 38°C. O ile kolejna fala upałów jest już w zasadzie przesądzona, o tyle należy być bardzo ostrożnym w przypadku tej prognozy z racji tego, że jest ona na granicy realnej prognozy dla modelu ECMWF (> T+144h). Osobiście prognozowalibyśmy raczej 36°C, nie 38°C. Jeśli taka prognoza pojawi się na T+48h, będziemy jednak mocno zaniepokojeni. Przy braku zachmurzenia i konwekcji mogłoby to bowiem oznaczać lokalnie temperatury jeszcze wyższe – model ECMWF często zaniża temperatury w czasie upałów."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 2) + ". " + "BAŁTYK RAZ JESZCZE", "Poniżej przebieg temperatur uśrednionych do obszaru 15-20E, 55-56N wg OiSSTv2 od września 1981 do 1 sierpnia 2018. W sposób oczywisty południowy Bałtyk jest obecnie najcieplejszy w historii pomiarów. Średnia temperatura obszarowa 1 sierpnia wzrosła tu do 23.7°C. Poprzednie rekordowe temperatury tego obszaru to i 22.2°C 30 lipca 1994 i 22.1°C 28 lipca 2006."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 3) + ". " + "Niemcy: rekordowo niski stan wód w rzekach ujawnia niewybuchy z drugiej wojny światowej", "Przeciągająca się fala upałów w Niemczech systematyczni obniża poziom wody w tutejszych rzekach, także na Łabie, odsłaniając niebezpieczne amunicje z czasów II wojny światowej. Policja ostrzega zaciekawionych mieszkańców przed dotkaniem granatów, min i innych potencjalnie niebezpiecznych materiałów, które w każdej chwili mogą wybuchnąć. Szczególnie dużo niewypałów znaleziono we wschodnich landach Saksonia-Anhalt oraz Saksonia, które były miejscem licznych bitew w czasie drugiej wojny światowej – informuje Deutsche-Welle."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 4) + ". " + "Lokalne burze nad Polską. Aktualizacja prognoz na noc", "Umiarkowane burze, nie żadne silne nawałnice jak twierdzi tefałen meteo – najgorsza stacja meteorologiczna w Polsce – przechodzą w sobotni wieczór nad kilkoma województwami Polski. Towarzyszy im mocniejszy deszcz oraz silniejszy wiatr, ale póki co nie ma zagrożenia szczególnie silnymi zjawiskami, które mogłyby przyczynić się do spustoszeń."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 5) + ". " + "Nad Polską zalega powietrze zwrotnikowe", "Prognoza pogody na dziś: gorąca sobota z burzami. Może pojawić się grad Termometry pokażą do 32 stopni na Dolnym Śląsku i Ziemi Lubuskiej. Prognozuje się przelotny deszcz i burze. Towarzyszyć im będzie porywisty wiatr. Zalega nad nami upalne powietrze pochodzenia zwrotnikowego (PZ). Warunki biometeorologiczne niekorzystne."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 6) + ". " + "KOMENTARZ SYNOPTYKA", "W godzinach rannych przeszedł przez Warszawę i Łódź front z godzinną burzą i opadem deszczu, front ciągnie się przez cały kraj, od Białegostoku po Zieloną Górę, chmury burzowe na nim szybko wędrują na wschód, zaś południowa składowa ruchu jest niewielka. Do godzin popołudniowych burze i opady dojdą do Podkarpacia, wieczorem i w nocy front wyjdzie poza południowe granice kraju. Front związany jest z przechodzącym przez północną Skandynawię serią niewielkich ośrodków niżowych, jeden z nich pojawia się na naszych prognozach ciśnienia nad Finlandią w rejonie Zatoki Botnickiej. Tak więc realizuje się odbudowa niezbyt silnej cyrkulacji zachodniej, która prowadzi do wymiany upalnej masy na bardziej świeżą masę znad Morza Północnego i Norweskiego."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 7) + ". " + "Gdy za oknem szaruga, porozmawiajmy o pogodzie [BLOG]", "Tegorocznego lata nie obserwujemy paraliżujących fal upałów. Charakteryzuje się ono raczej okresami intensywnych opadów deszczu z przejściowymi ochłodzeniami rozdzielającymi okresy przyjemnej, słonecznej pogody z umiarkowaną temperaturą. Rozmowa o pogodzie jest tak nieunikniona jak oddychanie, jednak głębia naszych dyskusji o letniej aurze nie sprowadza się do piękna kształtów chmur czy dobroci opadów a raczej do narzekań, że jest zbyt pochmurno i zbyt mokro."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 8) + ". " + "Wpływ pogody na żerowanie ryb", "Zacznijmy od początku. Od jakiegoś czasu, przed wyjazdem na ryby, regularnie sprawdzam szczegółowe prognozy pogody. Interesuje mnie temperatura powietrza, ciśnienie, siła i kierunek wiatru, zachmurzenie i opady (unikam łowienia w ulewie). Jedną z rzeczy, jakiej nauczyłem się dzięki temu o pogodzie jest to, że ciśnienie zmienia się niemal nieustannie. W zasadzie rzadko bywa, żeby dłużej niż 2-3 dni utrzymywało się jednakowe ciśnienie. Ba, wahania ciśnienia obserwujemy nawet w cyklu dobowym (mam barometr). Wiadomo, że zmiany tej wartości są raz większe raz mniejsze, ale jedno jest pewne – ciśnienie ciągle się zmienia. To istotne ustalenie. Jeżeliby bowiem każda zmiana ciśnienia działała negatywnie na ryby, to nie żerowałby by one niemal w ogóle."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 9) + ". " + "POGODA NA ZAKYNTHOS – NIESZABLONOWY PORADNIK TURYSTY", "Pewnie zastanawiacie się jaki miesiąc wybrać na swój niezwykły urlop na Zante? Jaka jest pogoda na Zakynthos? Nie tylko Wam chodzą po głowie takie myśli. Codziennie spotykamy się z takim pytaniem ze strony turystów. Nie ma jednoznacznej odpowiedzi ponieważ wszystko zależy od gustu, odporności organizmu na wysokie temperatury, preferencji w związku ze sposobami spędzania wolnego czasu oraz Waszych wyobrażeń o urlopie."));
                postRepository.save(new Post(null, Integer.toString(10 * i + 10) + ". " + "POGODA W LONDYNIE. CZY NAPRAWDĘ CIĄGLE PADA??", "Pogoda to dość kluczowy aspekt podczas wyboru miejsca na wakacje, ale też i miejsca zamieszkania. Bo skoro lubisz ciepło, słońce, a najlepiej plaże i morze… to po jaką cholerę mieszkasz w Londynie?! Często zadaję sobię to pytanie, gdy pogoda na polu iście „Angielska” – deszcz, mgła, wiatr. Tak więc odpowiedzmy sobie na jedno często zadawane pytanie, czy na Wyspach cały czas pada?? Jaka jest tak naprawdę pogoda w Londynie??"));
            });
        }
    }
}
