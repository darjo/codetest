package com.pierceecom.blog.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationErrors {

    private final List<FieldError> fieldsErrors = new ArrayList<>();

    public void addError(final String fieldName, final String errorMessage) {
        fieldsErrors.add(new FieldError(fieldName, errorMessage));
    }

    public boolean isEmpty() {
        return fieldsErrors.isEmpty();
    }

    public List<FieldError> getErrors() {
        return fieldsErrors;
    }
}
