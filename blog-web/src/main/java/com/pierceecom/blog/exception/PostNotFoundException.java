package com.pierceecom.blog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PostNotFoundException extends RuntimeException {

    private static final String MESSAGE = "post doesn't exist.";

    public PostNotFoundException() {
        super(MESSAGE);
    }
}
