package com.pierceecom.blog.model;

import com.pierceecom.blog.validation.PostTitleConstraint;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {

    @ApiModelProperty(dataType = "numeric", example = "1", notes = "post id", allowEmptyValue = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(dataType = "string", example = "what I did today", notes = "post title")
    @NotNull
    @Size(max = 250)
    @PostTitleConstraint
    @Column(nullable = false)
    private String title;

    @ApiModelProperty(dataType = "string", example = "wrote a boring post", notes = "post content")
    @NotNull
    @Size(min = 10, max = 1024)
    @Column(nullable = false)
    private String content;
}
