package com.pierceecom.blog.model;

import com.pierceecom.blog.exception.FieldError;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class Errors {

    private List<Error> errors;

    public Errors(final List<FieldError> fieldErrors) {
        errors = fieldErrors.stream().map(f -> new Error(f.getMessage(), new Source("data/attributes/" + f.getFieldName() + "-field"))).collect(Collectors.toList());
    }
}
