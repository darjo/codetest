package com.pierceecom.blog.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PostTitleValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PostTitleConstraint {

    String message() default "title should begins with capital letter or digit";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
