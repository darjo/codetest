package com.pierceecom.blog.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PostTitleValidator implements
        ConstraintValidator<PostTitleConstraint, String> {

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {

        return value != null && !value.isEmpty() && (Character.isUpperCase(value.charAt(0)) || Character.isDigit(value.charAt(0)));
    }
}
