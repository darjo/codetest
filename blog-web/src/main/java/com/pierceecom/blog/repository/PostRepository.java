package com.pierceecom.blog.repository;

import com.pierceecom.blog.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> {

    Iterable<Post> findAllByOrderByIdDesc();

    Page<Post> findAll(final Pageable pageable);

    Page<Post> findAllByOrderByIdDesc(final Pageable pageable);
}
