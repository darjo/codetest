package com.pierceecom.blog.controller;

import com.pierceecom.blog.model.Post;
import com.pierceecom.blog.model.Posts;
import com.pierceecom.blog.service.PostService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/posts")
@Transactional
class PostController {

    @Autowired
    private PostService postService;

    @ApiOperation(value = "Get all posts", response = Posts.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "successful operation")})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Posts list(@RequestParam(value = "page", required = false) final Integer page, @RequestParam(value = "per_page", required = false) final Integer perPage) {

        if (page == null) {
            return postService.findAll();
        } else {
            return postService.findPage(page, perPage);
        }
    }

    @ApiOperation(value = "Get all posts, returns simple array", response = Post[].class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "successful operation")})
    @GetMapping(value = "/simple", produces = MediaType.APPLICATION_JSON_VALUE)
    Iterable<Post> simple() {

        return postService.listAll();
    }

    @ApiOperation(value = "Add a new post", response = Post.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "OK of post"), @ApiResponse(code = 405, message = "invalid input")})
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Post> post(@Valid @RequestBody final Post body) {

        final Post post = postService.insert(body);

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(post.getId())
                .toUri();

        return ResponseEntity.created(location).body(post);
    }

    @ApiOperation(value = "Updates a post", response = Post.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "OK of post"), @ApiResponse(code = 404, message = "post not found"), @ApiResponse(code = 405, message = "invalid input")})
    @PutMapping(value = "/{postId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Post> put(@PathVariable final Long postId, @Valid @RequestBody final Post body) {

        final Post post = postService.update(postId, body);

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .build()
                .toUri();

        return ResponseEntity.created(location).body(post);
    }

    @ApiOperation(value = "Find post by ID", response = Post.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "successful operation"), @ApiResponse(code = 204, message = "no content")})
    @GetMapping(value = "/{postId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Post get(@PathVariable final Long postId) {

        return postService.findById(postId);
    }

    @ApiOperation(value = "Deletes a post")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "successful operation"), @ApiResponse(code = 404, message = "post not found")})
    @DeleteMapping(value = "/{postId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Post> delete(@PathVariable final Long postId) {

        postService.delete(postId);

        return ResponseEntity.noContent().build();
    }
}
