package com.pierceecom.blog.controller;

import com.pierceecom.blog.model.Message;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HelloPierceController {

    private static final String HELLO_PIERCE = "Hello Pierce";

    @ApiOperation(value = "Returns nice message", response = String.class)
    @GetMapping("/hello-pierce")
    Message hello() {
        return new Message(HELLO_PIERCE);
    }
}
