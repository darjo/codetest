package com.pierceecom.blog.controller;

import com.pierceecom.blog.exception.FieldError;
import com.pierceecom.blog.model.Errors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class RestExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Errors> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {

        final BindingResult result = ex.getBindingResult();
        final List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();

        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Errors(processFieldErrors(fieldErrors)));
    }

    private List<FieldError> processFieldErrors(final List<org.springframework.validation.FieldError> fieldErrors) {

        final List<FieldError> result = new ArrayList<>();
        for (final org.springframework.validation.FieldError e : fieldErrors) {
            logger.error(e.getField() + ": " + e.getDefaultMessage());
            result.add(new FieldError(e.getField(), e.getDefaultMessage()));
        }
        return result;
    }
}
