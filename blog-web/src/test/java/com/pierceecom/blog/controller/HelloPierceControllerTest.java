package com.pierceecom.blog.controller;


import com.pierceecom.blog.model.Message;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HelloPierceControllerTest {

    private HelloPierceController controller;

    @Before
    public void prepare() {

        controller = new HelloPierceController();
    }

    @Test
    public void messageTest() {

        Message result = controller.hello();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("Hello Pierce");
    }
}
